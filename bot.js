// Discord bot with YT Radio function and other miscellaneous functions

// Import libraries
import fs from 'fs';
import discord from 'discord.js';
import ytSearch from 'youtube-search';
import {htmlEscape, htmlUnescape} from 'escape-goat';
import ytdl from 'ytdl-core';
//import ytdl from 'ytdl-core-discord';
import deepl from 'deepl';

// read authentication tokens from local json file
var tokens = {};
fs.readFile("./auth.json", (err, data) => {
	if (err) {
		console.error("Error reading auth.json");
	}
	else {
		console.log("Read data OK");
		tokens = JSON.parse(data);
		client.login(tokens.discord_token);
		ytOpts.key = tokens.youtube_token;
	}
});

// Create Discord client
const client = new discord.Client();
// Youtube search options
var botChannel = null;
var ytOpts = {
	maxResults: 1,
	key: tokens.youtube_token
};

// 8ball
const eightBall_answers = [
							"That's absolutely true!",
							"Yeah, it's gonna happen",
						  	"It's gonna happen... some day",
						   	"Fifty-fifty.",
						  	"Probably true",
							"Most likely it's true... that it is not going to happen.",
							"Yeah, it's false hope.",
							"Nope.",
							"Not at all.",
							"Throw away all hope."];

// Prefix used for commands
const cmdPrefix = "pb!";


/* 
	Queue system
	Channels Map
	    -> Key: Guild Id	Value: Array (w/ Dispatcher, Playlist array, other settings)
		(Each channel has a different guild ID. Should work with multiple channels/servers)
*/
const CHANNELS = new Map();

// misc functions

async function playNextSong(msg, serverQueue) {
	console.log(`[RadioFunction] Moving forward in queue for ${msg.guild.id}`);

	var mainQueueObject = CHANNELS.get(msg.guild.id);

	// Move queue forward
	if (!mainQueueObject || mainQueueObject.songs.length < 1) {
		console.log(`[RadioFunction (GID: ${msg.guild.id})] No items in queue, cancel.`);
		msg.channel.send("There's no songs in the queue!");
		return;
	}
	else if (mainQueueObject.playing && msg.author.username === mainQueueObject.currentSong.req) {
		msg.channel.send("Okay, moving on to the next song!");
	}
	else if (mainQueueObject.playing) {
		msg.channel.send("We're already playing a song!");
		return;
	}

	let nextSong = mainQueueObject.songs.shift();
	mainQueueObject.currentSong = nextSong;
	let musicId = nextSong.id;
	let musicUrl = nextSong.url;

	console.log(`[RadioFunction (GID: ${msg.guild.id})] Voice Channel = ${msg.member.voice.channel}`);
	if (msg.member.voice.channel) {
			console.log("Before Await: " + mainQueueObject.connection);
			/*if (mainQueueObject.connection === undefined)*/ mainQueueObject.connection = await msg.member.voice.channel.join();
			console.log("After Await: " + mainQueueObject.connection);
			
			console.log("VoiceChannel Join");

			// download
			//dl: ytdl(musicUrl, { quality: 'highestaudio' }).pipe(fs.createWriteStream(`./dls/${musicId}.m4a`));

			const dispatcher = mainQueueObject.connection.play(await ytdl(musicUrl, { quality: 'highestaudio'}), { volume: 0.5 });
			//const dispatcher = mainQueueObject.connection.play(await ytdl(musicUrl, { type: 'opus' }), { volume: 0.5 });

			//dl: const dispatcher = mainQueueObject.connection.play(`./dls/${musicId}.m4a`);

			dispatcher.on('start', () => {
				mainQueueObject.playing = true;
				msg.channel.send("Now playing - " + nextSong.title);
				console.log("[RadioFunction] Dispatcher event start with musicID= " + musicId);
			}); // dp event start

			dispatcher.on('finish', () => {
				mainQueueObject.playing = false;
				console.log("[RadioFunction] Dispatcher finish with musicID= " + musicId);
				//msg.member.voice.channel.leave();
				//dispatcher.destroy();
				if (mainQueueObject.songs.length > 0) playNextSong(msg, serverQueue);

			}); // dp event finish

			dispatcher.on('error', (err) => {
				console.log("[RadioFunction] Error occurring... hm");
				console.log(err);
				//msg.member.voice.channel.leave();
				//dispatcher.destroy();
			}); // dp event error
			

	} // endif msg.member.voice.channel
	else {
		msg.reply("You should be in a voice channel before using this command! 🙄");
	}
}

// initialize bot in voice channel, NOT USED
async function initVoiceConnection(msg) {
	var connection = await msg.member.voice.channel.join();
	CONNECTION = connection;
}

// Discord listeners
client.on('ready', () => {
	console.log("[Discord] Logged in.");
});

client.on('message', msg => {
	console.log("[Discord] @" + msg.author.username + ": " + msg.content);

	// Server queue
	const serverQueue = CHANNELS.get(msg.guild.id);

	if (msg.content.startsWith(cmdPrefix + "q")) { // queue adder
		if (!serverQueue) {
			const queueConstruct = {
				textChannel: msg.channel,
				voiceChannel: msg.member.voice.channel,
				connection: undefined,
				songs: [],
				currentSong: '',
				search: [],
				volume: 5,
				playing: false //true
			};
	
			CHANNELS.set(msg.guild.id, queueConstruct);
		}

		if (!msg.content.startsWith(cmdPrefix + "q-id:")) {
			ytSearch(msg.content.substr((cmdPrefix + "q").length+1), ytOpts, function(err, results) {
				if (err) return console.log(err);
	
				let musicUrl = '';
				let musicId = '';
				let musicTitle = '';
				for (let i = 0; i < results.length; ++i) {
					const embed = new discord.MessageEmbed()
					.setTitle(htmlUnescape(results[i].title))
					.setColor(0xff0000)
					.setDescription(results[i].link)
					.setThumbnail(results[i].thumbnails.default.url)
					.addFields(
						{ name: 'Channel', value: results[i].channelTitle },
						{ name: 'Description', value: results[i].description }
					)
					.setFooter('Queued by ' + msg.author.username);
	
					musicUrl = results[i].link;
					musicId = results[i].id;
					musicTitle = results[i].title;
	
					//QUEUE.push( { id: musicId, url: musicUrl, req: msg.author.username });
					CHANNELS.get(msg.guild.id).songs.push({ id: musicId, url: musicUrl, title: htmlUnescape(musicTitle), req: msg.author.username });
					//console.log(QUEUE);
					msg.channel.send(embed);
				}
			});
		}
		else {
			CHANNELS.get(msg.guild.id).songs.push({ id: msg.content.substr((cmdPrefix + "q-id:").length+1), url: "https://www.youtube.com/watch?v=" + msg.content.substr((cmdPrefix + "q-id:").length+1), title: 'custom track by id', req: msg.author.username });
			msg.reply("Custom track added by ID to playlist queue.");
		}
		
	}
	else if (msg.content === cmdPrefix + "leave") {
		if (msg.member.voice.channel) msg.member.voice.channel.leave();
	}
	else if (msg.content.startsWith(cmdPrefix + "p")) {
		playNextSong(msg, serverQueue);
	}
	else if (msg.content.startsWith(cmdPrefix + "c")) {
		initVoiceConnection(msg);
	}
	//else if (msg.author.username.includes("Ate")) {
	//	msg.reply("Hey, what's up! 🤠").then(console.log).catch(console.error);
	//}
	else if (msg.content === cmdPrefix + "momo") {
		const embed = new discord.MessageEmbed()
				.setImage('https://lh3.googleusercontent.com/OEsKHxeu519JIyFLwcBU2NcVOCOdl2E5HIbF0CoZcVjqAe8MccJcoYEQxX-LUSx9nkNQc_65Dqb8PhZ4sc-ET4JYT_uRFtdu=w960-rj-nu-e365');

		msg.channel.send(embed);
	}
	else if (msg.content === cmdPrefix + "time") {
		msg.reply("Hey, the time now is " + new Date().toLocaleString()).then(console.log).catch(console.error);
	}
	else if (msg.content.startsWith(cmdPrefix + "8ball")) {
		let selectedAnswer = eightBall_answers[Math.floor(Math.random() * eightBall_answers.length)]; // ceiling, length+1 ?
		msg.reply(selectedAnswer).then(console.log).catch(console.error);
	}
	else if (msg.content.startsWith(cmdPrefix + "tl") && true === false) {
		// Requires API key which I do not have.
		// There's a free option but requires credit card anyway
		// no thanks
		// add api key to auth_key (prefer auth.json)
		// remove true === false from if statement to execute code

		console.log("Translation");
		let jaText = msg.content.substr((cmdPrefix + "tl").length+1).trim();

		deepl({
			free_api: true,
			text: jaText,
			target_lang: 'EN',
			auth_key: 'authkey'
		})
		.then(result => {
			const embed = new discord.MessageEmbed()
				.setTitle("Translation")
				.setColor(0xff0000)
				.addFields(
					{ name: 'Original text', value: jaText },
					{ name: 'Translation', value: result.data }
				)
				.setFooter('Translated by DeepL AI - Requested by ' + msg.author.username);
				
				msg.channel.send(embed);
		})
		.catch(error => {
			console.log(error);
		});
	}
});